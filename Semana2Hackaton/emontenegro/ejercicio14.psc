Proceso ejercicio14
	Definir montoCobrado Como Real
	Escribir "Algoritmo de descuentos"
	Escribir "Ingrese un Mes: "
	Leer mes
	Escribir "Ingrese un Importe: "
	Leer importe
	
	Si (mes > 0 y mes < 13) Entonces
		Si mes = 10 Entonces
			montoCobrado = importe * 0.85
		SiNo
			montoCobrado = importe
		FinSi
		
		Escribir "El monto cobrado es: ", montoCobrado
	SiNo
		Escribir "Ingrese un Mes Valido"
	FinSi

FinProceso