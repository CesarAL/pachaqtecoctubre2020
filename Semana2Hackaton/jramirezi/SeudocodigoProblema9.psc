//Escribir un Pseudoc�digo que encuentre y despliegue los n�meros primos entre uno y cien. 
//Un n�mero primo es divisible entre el mismo y la unidad por lo tanto un numero primo no puede ser par excepto el dos (2).
Proceso Primos
	Definir primo, divisores Como Entero
	Escribir " ======== Numeros Primos del 1 al 100 ===="
	primo = 1.
	
	Mientras primo<=100 Hacer 			
		divisores =0.			
				
			Para i=1 Hasta primo Con Paso 1 Hacer
				Si primo%i=0 Entonces
					divisores=divisores+1				
				Fin Si
			Fin Para
			
			Si divisores<=2 Entonces
				imprimir primo			
			Fin Si				
					
		primo = primo + 1.
	FinMientras				
				
FinProceso
