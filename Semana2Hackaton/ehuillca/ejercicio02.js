/** Escriba una funcion recursiva de javascript que determines si un numero es par o impar**/
function Par(valor){
    if (valor === 0)  return true;
    else return Impar(valor - 1);
}

function Impar(valor){
    if (valor === 0)  return false;
    else return Par(valor - 1);
}
    
function EsParOImpar(){
    numero=prompt("Ingresa un numero: ");   
    if(Par(Math.abs(numero))) console.log(numero + " es par");
    else console.log(numero + " es impar");
}
    
EsParOImpar();
