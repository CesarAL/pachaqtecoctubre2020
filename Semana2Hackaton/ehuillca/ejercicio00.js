function LimpiarCadena(cadena){
    variosEspacios=/[ ]+/g; //Expresion regular que identifica a muchos espacios en la cadena
    cadena=cadena.trim(); // limpiamos los espacios en blanco al inicio y al final
    cadena=cadena.replace(variosEspacios," "); 
    return cadena;
}


function CapitalizarCadena(cadena){
    cadena=LimpiarCadena(cadena);
    palabra=cadena.split(" "); //partimos la frase descpues de cada espacio
    palabraCapitalizada="";

    for(i=1; i < palabra.length; i++){    	
        palabraCapitalizada += palabra[i].charAt(0).toUpperCase() + palabra[i].slice(1)+" ";
    }
    return palabraCapitalizada.trim(); 
  
}
console.log(CapitalizarCadena("el peru es un pais muy rico en cultura"));
