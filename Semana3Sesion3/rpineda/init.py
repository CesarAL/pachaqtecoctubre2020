# print("hola", "como estas", "mi nombre es roberto")
# print("hola soy", end=" ")
# print("david lopez")

# nombre = input("Escribeme tu nombre ")
# print(f"mucho gusto {nombre}")

# edad = int(input("Dime tu edad, escribe en numeros "))
# try:
#     edad = int(input("Dime tu edad, escribe en numeros "))
#     print(f"que chevere que tengas {edad} años y la mitad de tu edad es {edad/2}")
# except ValueError:
#     print("No puedes escribir en letras tu edad")
# except Exception as error:
#     print(f"Por favor escribe tu edad en numeros")
#     print(f"Ocurrión un error, el error es: {error}")
# finally:
#     print("Se termino de ejecutar")


# try:
#     numero1 = int(input("Dime el primer numero para dividor "))
#     numero2 = int(input("Dime el segundo numero para dividor "))
#     print(f"el resultado es : {numero1/numero2}")
# except ValueError:
#     print("No puedes escribir en letras los numeros")
# except ZeroDivisionError:
#     print("no se puede dividir para cero")
# except Exception as error:
#     print(f"Ocurrión un error, el error es: {error}")
# finally:
#     print("Se termino de ejecutar")

#################### IF #############################
#Pedir el valor unitario
#multiplicar el valor unitario  por la cantidad
#Evaluar cuanto vamos a pagar de impuesto a la renta
# try:
#     valorUnitario = int(input("Ingresa el valor unitario "))
#     cantidad = int(input("ingresa la cantidad "))
#     baseImponible = 50000
#     impuesto = 0
#     porcentajeImpuesto = 4
#     subtotal = valorUnitario * cantidad
#     if(subtotal >= baseImponible):
#         impuesto = (subtotal * porcentajeImpuesto)/100
#         total = subtotal + impuesto
#     else:
#         total = subtotal

#     print(f"El total a pagar es: {total}, tu impuesto es: {impuesto}, el subtotal es {subtotal}")
# except Exception as error:
#     print(error)

###################3 while ##############3
# numero = 0
# while numero <= 50:
#     print(numero)
#     numero = numero + 1
# detener = True
# while detener:
#     try:
#         numero = int(input("Escribe un numero "))
#         detener = False
#     except Exception as error:
#         print("Este no es un numero")
##################3 FOR #########################
lstNombres = ["Melanny", "Lucia", "Deysi", "Fernanda"]
lstNombres.append("Geovanna")
print(lstNombres)

for y in lstNombres:
    print(y)
    if(y == "Deysi"):
        print(f"Hola {y}")
    else:
        print(f"Chao {y}")

for numero in range(-50, 52, 3):
    print(numero)
    if(numero == 28):
        print(numero)
        break


# operacion = input("Escribe la operacion ")
# if(operacion == "Suma"):
#     numero1 = int(input("Escribe el primero numero "))
#     numero2 = int(input("Escribe el segundo numero "))
#     respuesta = numero1 + numero2
#     print(f"La respuesta es: {respuesta}")
# if(operacion == "Resta"):
#     numero1 = int(input("Escribe el primero numero "))
#     numero2 = int(input("Escribe el segundo numero "))
#     respuesta = numero1 - numero2
#     print(f"La respuesta es: {respuesta}")
# else:
#     print("operacion no implementada")

