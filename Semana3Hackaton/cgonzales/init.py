import ejercicios
import msvcrt

opcion = -1
opciones = ['Mostrar Edad y Peso', 'Calcular Area del Triangulo', 'Calcular Area del Circulo',
            'Division del Mayor', 'El Mayor de 3 Numeros', 'Mostrar La Fecha', '¿Usted Puede Votar?',
            'Nomina Salarial', 'Ver los Numeros Primos al 100', 'Calcular Factorial',
            'Tablas de Multiplicar', 'Maximo Minimo y Media', 'Secuencia de Numeros',
            'Importe a Cobrar s/c descuento', 'Diez Numeros']

while opcion != 0:
    for x in opciones:
        print (f'({opciones.index(x) + 1}) ........... {x}')
    try:
        opcion = int (input ('Ingresar una opcion del 1 - 15. O 0 para salir '))
    except ValueError:
        opcion = -1
    if opcion == 0:
        print ('EL PROGRAMA SE CERRARA...!')
        #break
    elif opcion == -1:
        print ('Valores del tipo no numerico no se permiten')
    elif opcion == 1:
        ejercicios.EdadPeso ()
    elif opcion == 2:        
        ejercicios.AreaTriangulo ()
    elif opcion == 3:
        ejercicios.AreaCirculo ()
    elif opcion == 4:
        ejercicios.DividirMayor ()
    elif opcion == 5:
        ejercicios.MayorDeTres ()
    elif opcion == 6:
        ejercicios.FormatoFecha ()
    elif opcion == 7:
        ejercicios.PuedeVotar ()
    elif opcion == 8:
        ejercicios.CalcularSueldo ()
    elif opcion == 9:
        ejercicios.NumerosPrimos ()
    elif opcion == 10:
        ejercicios.CalcularFactorial ()
    elif opcion == 11:
        ejercicios.TablasMultiplicar ()
    elif opcion == 12:
        ejercicios.MaximoMinimoMedia ()
    elif opcion == 13:
        ejercicios.SecuenciaNumeros ()
    elif opcion == 14:
        ejercicios.VoucherCliente ()
    elif opcion == 15:
        ejercicios.EvaluarDiez ()
    else:
        print ('Ingrese un valor de la lista')
    print ('Oprima cualquier tecla para continuar')
    msvcrt.getch ()